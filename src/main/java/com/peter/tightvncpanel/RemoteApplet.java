package com.peter.tightvncpanel;

import java.awt.BorderLayout;

import javax.swing.JApplet;
import javax.swing.JPanel;

public class RemoteApplet extends JApplet {

	public RemoteApplet() {
		getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		TightVNC.initVNCPanel(null, panel, "p", 5903, "abcd");
	}

}
