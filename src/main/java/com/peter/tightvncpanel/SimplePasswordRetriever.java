package com.peter.tightvncpanel;

import com.glavsoft.rfb.IPasswordRetriever;

public class SimplePasswordRetriever implements IPasswordRetriever {
	String password;

	public SimplePasswordRetriever(String password) {
		this.password = password;
	}

	@Override
	public String getPassword() {
		return password;
	}

}
