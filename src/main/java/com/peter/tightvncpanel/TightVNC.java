package com.peter.tightvncpanel;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.Logger;

import com.glavsoft.rfb.protocol.Protocol;
import com.glavsoft.rfb.protocol.ProtocolSettings;
import com.glavsoft.viewer.ConnectionPresenter;
import com.glavsoft.viewer.UiSettings;
import com.glavsoft.viewer.Viewer;
import com.glavsoft.viewer.swing.ConnectionParams;
import com.glavsoft.viewer.swing.LocalMouseCursorShape;
import com.glavsoft.viewer.swing.Surface;
import com.glavsoft.viewer.swing.SwingConnectionWorkerFactory;
import com.glavsoft.viewer.swing.SwingViewerWindow;
import com.glavsoft.viewer.swing.SwingViewerWindowFactory;
import com.glavsoft.viewer.swing.gui.ConnectionView;

public class TightVNC extends JFrame implements WindowListener {
	public static boolean enableLogger = false;
	public static boolean showJOptionPane = false;
	private JPanel contentPane;
	private Viewer viewer;
	JPanel vncPanel = new JPanel();
	static Logger logger = Logger.getLogger(TightVNC.class);

	public static void main(String[] args) {
		logger.info(PropertyUtil.getProperty("version"));
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TightVNC frame = new TightVNC();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TightVNC() {
		setTitle("TightVNC");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1132, 721);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		final JPanel vncPanel = new JPanel();
		contentPane.add(vncPanel, BorderLayout.CENTER);
		//initVNCPanel(this, vncPanel, "127.0.0.1", 5900, "");
		TightVNC.initVNCPanel(TightVNC.this, vncPanel, "p", 5904, "fuck!QAZ");
//		new Thread() {
//			public void run() {
//				try {
//					Thread.sleep(10000);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//				TightVNC.initVNCPanel(TightVNC.this, vncPanel, "p", 5904, "");
//			}
//		}.start();
	}

	public SwingViewerWindow createViewerWindow(Protocol workingProtocol, ProtocolSettings rfbSettings, UiSettings uiSettings, String connectionString,
			ConnectionPresenter presenter) {
		Surface surface = new Surface(workingProtocol, uiSettings.getScaleFactor(), uiSettings.getMouseCursorShape());
		final SwingViewerWindow viewerWindow = new SwingViewerWindow(workingProtocol, rfbSettings, uiSettings, surface, true, true, viewer, connectionString, presenter);
		surface.setViewerWindow(viewerWindow);
		viewerWindow.setRemoteDesktopName(workingProtocol.getRemoteDesktopName());
		rfbSettings.addListener(viewerWindow);
		uiSettings.addListener(surface);
		return viewerWindow;
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	public static void initVNCPanel(WindowListener windowListener, JPanel vncPanel, String host, int port, String password) {
		initVNCPanel(windowListener, vncPanel, host, port, password, false);
	}

	public static void initVNCPanel(WindowListener windowListener, JPanel vncPanel, String host, int port, String password, boolean needReconnection) {
		Viewer viewer = new Viewer();
		try {
			ProtocolSettings rfbSettings = ProtocolSettings.getDefaultSettings();

			UiSettings uiSettings = new UiSettings();
			uiSettings.setMouseCursorShape(LocalMouseCursorShape.DOT);
			uiSettings.setFullScreen(false);
			uiSettings.setScalePercent(100);

			boolean hasJsch = true;
			boolean allowInteractive = false;//allowAppletInteractiveConnections || ! isApplet;

			//			if (vncPanel.getClientProperty("connectionPresenter") != null) {
			//				ConnectionPresenter connectionPresenter = (ConnectionPresenter) vncPanel.getClientProperty("connectionPresenter");
			//				Viewer viewer2 = (Viewer) vncPanel.getClientProperty("viewer");
			//				connectionPresenter.setNeedReconnection(false);
			//				connectionPresenter.cancelConnection();
			//				viewer2.closeApp();
			//			}

			ConnectionPresenter connectionPresenter = new ConnectionPresenter(hasJsch, allowInteractive);
			ConnectionParams connectionParams = new ConnectionParams();
			connectionParams.hostName = host;
			connectionParams.parseRfbPortNumber(String.valueOf(port));
			connectionPresenter.addModel("ConnectionParamsModel", connectionParams);
			connectionPresenter.setNeedReconnection(needReconnection);
			final ConnectionView connectionView = new ConnectionView(windowListener, connectionPresenter, hasJsch);

			connectionPresenter.addView(ConnectionPresenter.CONNECTION_VIEW, connectionView);

			SwingViewerWindowFactory viewerWindowFactory = new SwingViewerWindowFactory(true, false, viewer);
			viewerWindowFactory.vncPanel = vncPanel;
			connectionPresenter.setConnectionWorkerFactory(new SwingConnectionWorkerFactory(connectionView.getFrame(), password, connectionPresenter, viewerWindowFactory));
			connectionPresenter.startConnection(rfbSettings, uiSettings, 0);

			//			vncPanel.putClientProperty("connectionPresenter", connectionPresenter);
			//			vncPanel.putClientProperty("viewer", viewer);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
